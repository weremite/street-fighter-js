import { controls } from '../../constants/controls';
import { createElement } from '../helpers/domHelper';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    const getHealthIndicator = document.getElementsByClassName('arena___health-indicator');
    const healthIndicator = [ ...getHealthIndicator ];
    const getHealthBar = document.getElementsByClassName('arena___health-bar');
    const healthBars = [ ...getHealthBar ];

    const statusInfo = {
      block: false,  
      currentHealth: 100,
      criticalStrikeTime: Date.now(),
      criticalStrikeInput: []
    }

    const playerOne = { 
      ...firstFighter, 
      ...statusInfo, 
      healthBar: healthBars[0], 
      healthIndicator: healthIndicator[0],
      position: 'left'
    }

    const playerTwo = { 
      ...secondFighter, 
      ...statusInfo, 
      healthBar: healthBars[1], 
      healthIndicator: healthIndicator[1],
      position: 'right'
    }

    function showStatus(fighter, text) {
      if(document.getElementById(`${fighter.position}-show-status`)) {
        document.getElementById(`${fighter.position}-show-status`).remove();
      }

    const showStatus = createElement({ 
      tagName: 'div', 
      className: 'arena___show-status', 
      attributes: {id: `${fighter.position}-show-status`} 
    });
      showStatus.innerText = text;
      showStatus.style.opacity = '1';
      fighter.healthIndicator.append(showStatus);
      setInterval(() => {
        if(showStatus.style.opacity > 0) {
          showStatus.style.opacity = showStatus.style.opacity - 0.01;
        } else {
          showStatus.remove();
        }
      }, 20);
    }

    function attackRelease(attacker, defender) {
      if(attacker.block) {
        showStatus(attacker, 'Release block to attack!');
        return void 0;
      }

      if(defender.block) {
        showStatus(defender, 'Blocked!');
        return void 0;
      }

      const totalDamage = getDamage(attacker, defender);

      if(!totalDamage) {
        showStatus(attacker, 'Missed!');
        return void 0;
      }

      if(attacker.criticalStrikeInput.length === 3) {
        showStatus(attacker, 'Critical hit!');
      }

      showStatus(defender, `-${totalDamage.toFixed(1)}`);
      defender.currentHealth = defender.currentHealth - totalDamage / defender.health * 100;
      if(defender.currentHealth < 0) {
        document.removeEventListener('keydown', onDown);
        document.removeEventListener('keyup', onUp);
        resolve(attacker);
      }

      defender.healthBar.style.width = `${defender.currentHealth}%`;
    }

    function critHandler(fighter) {
      const currentTime = Date.now();

      if(currentTime - fighter.criticalStrikeTime < 10000) {
        return false;
      }

      if(!fighter.criticalStrikeInput.includes(event.code)) {
        fighter.criticalStrikeInput.push(event.code);
      }

      if(fighter.criticalStrikeInput.length === 3) {
        fighter.criticalStrikeTime = currentTime;
        return true;
      }
    }

    function onDown(event) {
      if(!event.repeat) {
        switch(event.code) {
          case controls.PlayerOneAttack: {
            attackRelease(playerOne, playerTwo);
            break;
          }

          case controls.PlayerTwoAttack: {
            attackRelease(playerTwo, playerOne);
            break;
          }

          case controls.PlayerOneBlock: {
            playerOne.block = true;
            break;
          }

          case controls.PlayerTwoBlock: {
            playerTwo.block = true;
            break;
          }
        }

        if(controls.PlayerOneCriticalHitCombination.includes(event.code)) {
          critHandler(playerOne) ? attackRelease(playerOne, playerTwo) : null;
        }

        if(controls.PlayerTwoCriticalHitCombination.includes(event.code)) {
          critHandler(playerTwo) ? attackRelease(playerTwo, playerOne) : null;
        }
      }
    }

    function onUp(event) {
      switch(event.code) {
        case controls.PlayerOneBlock: playerOne.block = false; break;
        case controls.PlayerTwoBlock: playerTwo.block = false; break;
      }

      if(playerOne.criticalStrikeInput.includes(event.code)) {
        playerOne.criticalStrikeInput.splice(playerOne.criticalStrikeInput.indexOf(event.code), 1);
      }

      if(playerTwo.criticalStrikeInput.includes(event.code)) {
        playerTwo.criticalStrikeInput.splice(playerTwo.criticalStrikeInput.indexOf(event.code), 1);
      }
    }

    document.addEventListener('keydown', onDown);
    document.addEventListener('keyup', onUp);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = fighter.criticalStrikeInput === 3 ? 2 : Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
