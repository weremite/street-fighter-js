import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  function createFighterProperty(keyValue) {
    const propertyElement = createElement({
      tagName: 'div', 
      className: 'fighter-preview___property'
    });
    propertyElement.innerText = keyValue.join(': ').toUpperCase();

    return propertyElement;
  }

  function createFighterPreviewImage(source) {
    const attributes = {
      src: source
    };
    const imageElement = createElement({
      tagName: 'img', 
      className: 'fighter-preview___preview-image',
      attributes
    });

    if(position === 'right') {
      imageElement.style.transform = 'scaleX(-1)';
    }

    return imageElement;
  }

  if(fighter) {
    const keyValueArray = Object.entries(fighter);
    fighterElement.append(createFighterPreviewImage(fighter['source']));
    keyValueArray.filter(keyValueAll => keyValueAll[0] !== '_id' && keyValueAll[0] !== 'source')
    .forEach(keyValue => fighterElement.append(createFighterProperty(keyValue)));
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
