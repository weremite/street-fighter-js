import { showModal } from "./modal"

export function showWinnerModal(fighter) {
  // call showModal function 
  const showWinner = {
    title: "Congratulations!!!",
    bodyElement: `Our new Champ is ${fighter.name}`
  }

  showModal(showWinner);
}

